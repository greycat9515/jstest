import fs from 'fs'
import fse from 'fs-extra'
import http from 'axios'
import md5 from 'md5'
import qs from 'qs'
import FormData from 'form-data'
import zipdir from 'zip-dir'
import path from 'path'
import dotenv from 'dotenv'

dotenv.config({
  path: path.resolve(__dirname, `../../.env`)
})

const isPrepare = !!process.env.PREPARE_ONLY

const appDirectory = path.resolve(__dirname, '..')
const tmpDirectory = `${appDirectory}/tmp`

const amoSubDomain = process.env.AMO_SUBDOMAIN;
const amoUser = process.env.AMO_USER;
const amoHash = process.env.AMO_HASH;
const amoZone = process.env.AMO_ZONE;

const amoWidgetDirectory = fs.realpathSync(`${appDirectory}/skeleton`)

const getManifest = () => {
  return JSON.parse(fs.readFileSync(`${amoWidgetDirectory}/manifest.json`, 'utf8'))
}

const amoWidgetAlias = getManifest().widget.code || 'devio_sample'

if (!fs.existsSync(tmpDirectory)) {
  fse.ensureDirSync(tmpDirectory, parseInt('0777', 8))
}

const compressedBuildPath = tmpDirectory + '/widget.zip'

const _error = (text) => {
  console.log(text)
}
const _info = (text) => {
  console.log(text)
}
const _success = (text) => {
  console.log(text)
}

function getWidgetsList() {

  return new Promise((success, failure) => {
    http.get(`https://${amoSubDomain}.amocrm.${amoZone}/ajax/settings/dev`, {
      headers: {
        'X-Requested-With': 'XMLHttpRequest'
      },
      params: {
        USER_LOGIN: amoUser,
        USER_HASH: amoHash
      }
    })
      .then(response => {
        success(response.data.response.widgets.items)
      })
      .catch(e => {
        failure(e)
      })
  })

}

function getWidget() {
  _info(`Ищем виджет в amoCRM`)

  return new Promise((success, failure) => {
    getWidgetsList().then(widgets => {
      success(widgets.find(widget => widget.code === createCode()))
    }, error => {
      failure(error)
    }).catch(e => {
      console.trace(e);
      failure()
    })
  })
}

function create() {

  return new Promise((success, failure) => {

    try {

      http({
        method: 'POST',
        url: `https://${amoSubDomain}.amocrm.${amoZone}/ajax/settings/dev`,
        headers: {
          'content-type': 'application/x-www-form-urlencoded',
          'X-Requested-With': 'XMLHttpRequest'
        },
        params: {
          USER_LOGIN: amoUser,
          USER_HASH: amoHash
        },
        data: qs.stringify({
          action: 'create',
          code: createCode()
        })
      }).then(response => {
        if (response.data.error) {
          _error(`Ошибка при создании виджета, ответ сервера:\n${response.data.error}`)
          failure(response.data.error)
        } else {
          _success(`Виджет успешно создан в amoCRM`)

          getWidget().then(widget => {
            if (widget) {
              success(widget)
            }
            else {
              failure(`Ошибка при создании виджета`)
            }
          }, error => {
            failure(error)
          })
        }
      })


    } catch (e) {
      _error(`Ошибка при отправке запроса на создание виджета`)
      failure(e)
    }

  })

}

function prepare() {

  _info(`Копируем файлы из виджета в tmp-директорию`)
  fse.copySync(amoWidgetDirectory, tmpDirectory + '/' + amoWidgetAlias)

  const productionUrl = process.env.WEBPACK_PUBLIC_PATH.replace(/\/+$/,'') + '/bundle.js'
  const developmentUrl = process.env.WEBPACK_DEV_SERVER.replace(/\/+$/,'') + '/bundle.js'
  const widgetPath = `localStorage.getItem('debug_${amoWidgetAlias}')==='N'?'${productionUrl}':'${developmentUrl}'`

  _info(`Подменяем @@path и @@alias на ${widgetPath}`)
  let script = fs.readFileSync(`${tmpDirectory + '/' + amoWidgetAlias}/script.js`, 'utf8')
  .replace(/\`@@path\`/ig, widgetPath)
  .replace(/\`@@alias\`/ig, `${amoWidgetAlias}_define`)

  fs.writeFileSync(`${tmpDirectory + '/' + amoWidgetAlias}/script.js`, script);

  let manifest = getManifest()

  manifest.widget.version = manifest.widget.version || "1.0.0"
  manifest.widget.code = ''
  manifest.widget.secret_key = ''

  fs.writeFileSync(`${tmpDirectory + '/' + amoWidgetAlias}/manifest.json`, JSON.stringify(manifest))

  _info(`Замена данных manifest.json произведена`)

  compress().then(ok => {
    _success(`Архив сформирован`)
    process.exit()
  }, fail => {
    _error(`Ошибка при формировании архива`, fail)
    process.exit()
  })
}

function upload() {

  return new Promise((success, failure) => {

    _info(`Копируем файлы из виджета в tmp-директорию`)
    fse.copySync(amoWidgetDirectory, tmpDirectory + '/' + amoWidgetAlias)
    fse.copySync(`${appDirectory}/dist`, tmpDirectory + '/' + amoWidgetAlias)

    const productionUrl = process.env.WEBPACK_PUBLIC_PATH.replace(/\/+$/,'') + '/bundle.js'
    const developmentUrl = process.env.WEBPACK_DEV_SERVER.replace(/\/+$/,'') + '/bundle.js'
    const widgetPath = `localStorage.getItem('debug_${amoWidgetAlias}')==='N'?'${productionUrl}':'${developmentUrl}'`

    _info(`Подменяем @@path и @@alias на ${widgetPath}`)
    let script = fs.readFileSync(`${tmpDirectory + '/' + amoWidgetAlias}/script.js`, 'utf8')
      .replace(/\`@@path\`/ig, widgetPath)
      .replace(/\`@@alias\`/ig, `${amoWidgetAlias}_define`)

    fs.writeFileSync(`${tmpDirectory + '/' + amoWidgetAlias}/script.js`, script);

    (new Promise((success, failure) => {
      getWidget().then(widget => {
        if (!widget) {
          _info(`Виджет не обнаружен в amoCRM, создаем его`)
          create().then(widget => {
            success(widget)
          }, error => {
            failure(error)
          })
        }
        else {
          _info(`Виджет уже создан в amoCRM, обновляем его`)
          success(widget)
        }
      }, error => {
        failure(error)
      })
    })).then(widget => {

      let manifest = getManifest()

      manifest.widget.version = process.env.AMO_WIDGET_VERSION || "1.0.1"
      manifest.widget.code = widget.code
      manifest.widget.secret_key = widget.secret_key

      fs.writeFileSync(`${tmpDirectory + '/' + amoWidgetAlias}/manifest.json`, JSON.stringify(manifest))

      _info(`Замена данных manifest.json произведена`)
      _success(`Код виджета: ${widget.code}`)
      _success(`Секрет виджета: ${widget.secret_key}`)

      compress().then(ok => {
        send(manifest, widget).then(ok => {
          success({
            widget: {
              id: widget.id,
              code: widget.code,
              secret: widget.secret_key
            }
          })
        }, fail => {
          failure(fail)
        })
      }, fail => {
        failure(fail)
      })

    })

  })
}

function send(manifest, widget) {

  return new Promise((success, failure) => {

    try {

      let form = new FormData()

      form.append('widget', manifest.widget.code)
      form.append('secret', manifest.widget.secret_key)

      form.append('amouser', amoUser)
      form.append('amohash', amoHash)

      form.append('widget', fs.createReadStream(`${compressedBuildPath}`))

      http.post(`https://widgets.amocrm.${amoZone}/${amoSubDomain}/upload`, form, {
        headers: form.getHeaders()
      }).then(response => {

        try {
          response = JSON.parse(response.data.match(/{.*}/))

          if (response.response) {

            http.post(`https://${amoSubDomain}.amocrm.${amoZone}/ajax/widgets/edit/?USER_LOGIN=${amoUser}&USER_HASH=${amoHash}`, {
              action: 'edit',
              id: widget.id,
              code: widget.code,
              widget_active: 'Y',
              settings: {
                config: '...'
              }
              // 'settings[name]': '...'
            }, {
              headers: {
                'X-Requested-With': 'XMLHttpRequest'
              }
            });

            _info(`Виджет загрузен успешно`)

            // fse.removeSync(`${compressedBuildPath}`);
            // fse.removeSync(`${tmpDirectory}/${amoWidgetAlias}`);

            success(response)
          } else {
            _error(`Ошибка загрузки виджета, ответ:\n"${JSON.stringify(response)}"`)
            failure(JSON.stringify(response))
          }
        } catch (e) {
          _error(`Ошибка чтения ответа при загрузке виджета:\n${e}`)
          failure(e)
        }

      })

    } catch (e) {
      _error(`Ошибка чтения ответа при загрузке виджета:\n${e}`)
      failure(e)
    }

  })

}

function compress() {

  return new Promise((resolve, reject) => {

    try {
      fse.removeSync(`${compressedBuildPath}`);
    } catch (e) {}

    zipdir(`${tmpDirectory}/${amoWidgetAlias}`, {
      saveTo: `${compressedBuildPath}`
    }, error => {
      if (error) {
        _error(`Ошибка сжатия: ${error}`)
        reject(false)
      } else {
        fse.removeSync(`${tmpDirectory}/${amoWidgetAlias}`);
        _success(`Архив успешно создан`)
        resolve(true)
      }
    })

  })

}

function createCode() {
  let id = md5(amoSubDomain).slice(0, 5) + '_' + md5(amoUser).slice(0, 5)

  return `devio_${amoWidgetAlias}_${id}`
}

if (isPrepare) {
  prepare()
} else {
  upload()
}
