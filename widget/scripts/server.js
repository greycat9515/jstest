import webpack from 'webpack'
import config from '../config/webpack.config'
import webpackDevServer from 'webpack-dev-server'

const serverOptions = config.devServer

webpackDevServer.addDevServerEntrypoints(config, serverOptions)

const compiler = webpack(config)
const server = new webpackDevServer(compiler, serverOptions)

console.log(`Starting web server - ${process.env.WEBPACK_DEV_SERVER_HOST}:${process.env.WEBPACK_DEV_SERVER_PORT}`)

server.listen(process.env.WEBPACK_DEV_SERVER_PORT, process.env.WEBPACK_DEV_SERVER_HOST, err => {
  if (err) {
    console.trace(err)
  }

  console.log(`Webpack frontend: сервер запущен`)
})
