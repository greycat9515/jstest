import webpack from 'webpack'
import config from '../config/webpack.config'

webpack(config).run((err, stats) => {
  if (err) {
    console.trace(err);
  }

  process.exit(0)
})
