import WebSocketServer from 'ws'
import qs from 'qs'
import get from 'lodash/get'

export class Logger {

  constructor(props) {
    this.history = {}
    this.maxHistory = 10

    this.server = new WebSocketServer.Server({
      host: props.host,
      port: props.port
    })

    this.server.on('connection', (client, req) => {
      client.channel = +get(qs.parse(req.url.split('?')[1] || ''), 'channel', false) || 'all'

      if (!client.channel) {
        client.terminate()
      }

      const channelId = get(client, 'channel', 'all')

      client.send(`Last ${this.maxHistory} events for ${channelId}:`)

      for (const message of (this.history[channelId]||[])) {
        this.socketSend(client, message)
      }
    })
  }

  prepareMsg (message) {
    return `[${message.type}] ${message.date.toLocaleDateString([], {
      timeZone: 'Europe/Moscow'
    })} ${message.date.toLocaleTimeString([], {
      hour12: false
    })}: ${message.text}`
  }

  socketSend (client, message) {
    const clientProject = get(client, 'channel', false)
    const optsProject = get(message, 'opts.channel', false)

    if (
      (clientProject && (+optsProject === +clientProject)) ||
      !clientProject
    ) {
      const msg = this.prepareMsg(message)

      client.send(msg)
    }
  }

  send(type, text, opts = {}) {
    const optsProject = +get(opts, 'channel', false)||'all'

    if (typeof text !== 'string') {
      text = JSON.stringify(text)
    }

    if (process.env.DEBUG === 'Y') {
      console[type === 'INFO' ? 'log' : 'error'](text)
    }

    text = `${optsProject} - ${text}`

    const message = {
      date: new Date(),
      type, text, opts
    }

    this.pushHistory(message)

    if (process.env.NODE_ENV !== 'prod') {
      console.log(this.prepareMsg(message))
    }

    for (const client of this.server.clients) {
      try {

        this.socketSend(client, message)
      } catch (e) {
        client.terminate()
      }
    }
  }

  success(text, opts = {}) {
    this.send(`SUCCESS`, text, opts)
  }

  error(text, opts = {}) {
    this.send(`ERROR`, text, opts)
  }

  info(text, opts = {}) {
    this.send(`INFO`, text, opts)
  }

  pushHistory(message) {
    const channelId = get(message, 'opts.channel', 'all')

    if (!this.history[channelId]) {
      this.history[channelId] = []
    }

    this.history[channelId].push(message)

    if (this.history[channelId].length > this.maxHistory) {
      this.history[channelId].splice(0, this.history[channelId].length - this.maxHistory)
    }
  }
}
